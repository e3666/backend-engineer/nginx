# SUMÁRIO

- [**O que é nginx**](#o-que-é-nginx)
- [**WebServer**](#webserver)
    - [Exemplo 01](#exemplo-01)
    - [Exemplo 02](#exemplo-02)
    - [Exemplo 03](#exemplo-03)
    - [Exemplo 04](#exemplo-04)
- [**Load Balancer Layer 7**](#load-balancer-layer-7)
    - [Exemplo 01](#exemplo-01)
    - [Exemplo 02](#exemplo-02)
    - [Exemplo 03](#exemplo-03)
    - [Exemplo 04](#exemplo-04)
- [**Load Balancer Layer 4**](#load-balancer-layer-4)
    - [Exemplo 01](#exemplo-01)
- [**Publicando aplicação**](#publicando-aplicação)
    - [Instalando Docker](#instalando-docker)
    - [Exemplo 01](#exemplo-01)
    - [Domínio](#domínio)
    - [Criando certificado HTTPs](#criando-certificado-https)
    - [Exemplo 02](#exemplo-02)
- [**Referência**](#referência)


# O que é nginx

O nginx pode ser um web server, e uma proxy. E por ser uma proxy, ele pode ser um load balancer, router de backend, e cache.

O nginx se encaixa entre a aplicação e o client, assim todas as requests que o client faz passam por ele, e ele pode decidir para qual servidor da aplicação enviar.


# WebServer

O NginX funciona como WebServer, e você pode ler mais sobre o assinto no link abaixo

- [WebServer](https://gitlab.com/e3666/backend-engineer/web-server)

E agora eu darei exemplos de como utilizar o nginx com essa funcionalidade, e todos os exemplos estarão utilizando docker.

## Exemplo 01

Os arquivos para esse exemplo estão na pasta [exemplo-01](./WebServer/exemplo-01).


Para esse primeiro exemplo vou fazer o nginx como webserver de arquivos estáticos, para isso vou criar um arquivo `index.html` que irei jogar para dentro da pasta `/etc/nginx` do container (poderia ser qualquer lugar, só preciso referenciar corretamente), e configurar o arquivo `nginx.conf` da seguinte forma:

```conf
http {
    server {
        listen 80;
        root /etc/nginx/;
    }
}

events { }
```

Essa configuração me da um WebServer por conta do http, e dentro desse bloco eu posso criar vários servers, mas eu quis criar somente um. Que fica ouvindo na porta 80 do container, e envia a request para o caminho `/etc/nginx/`. E por padrão pega o arquivo `index.html`.

Agora você pode acessar `localhost:8080` e verá meu `olá` do html. 

> OBS.: Eu espelhei a porta 80 do container na minha 8080

## Exemplo 02

Os arquivos para esse exemplo estão na pasta [exemplo-02](./WebServer/exemplo-02).

Mas agora vamos criar um servidor um pouco mais elaborado, que irá rotear várias páginas estáticas com base na localização delas, ou seja, se digitarmos `localhost:8080/images` vamos ver a página de images, se digitarmos `localhost:8080/pdf` vamos ver a página de pdfs.

Para isso, eu criei uma pasta chamada `my_site` que tem duas pastas, images e pdf. E vou colocar essa pasta dentro do diretório `/etc/nginx/`, e agora o arquivo de configuração do nginx fica:

```conf
http {
    server {
        listen 80;
        root /etc/nginx/my_site;
    }
}

events { }
```

E agora você pode acessar os links:

- `http://localhost:8080/images/`
- `http://localhost:8080/pdf/`

## Exemplo 03

Os arquivos para esse exemplo estão na pasta [exemplo-03](./WebServer/exemplo-03).


Agora vamos bloquear requests para certos caminhos utilizando regex. Como exemplo, irei bloquear requests para qualquer arquivo `.png`, para isso eu vou deixar um arquivo de imagem salvo na pasta de images.


```conf
http {
    server {
        listen 80;
        root /etc/nginx/my_site;
        
        location ~ .png$ {
            return 403;
        }
    }
}

events { }
```

E assim quando você tentar acessar `http://localhost:8080/images/python-logo.png` você recebera o erro 403 Forbidden.

## Exemplo 04

Os arquivos para esse exemplo estão na pasta [exemplo-04](./WebServer/exemplo-04).


Agora vou fazer um exemplo de `proxy_pass`, que irá funcionar da seguinte forma: Terei um outro servidor na porta 8888, e caso alguém acesse essa porta em /, eu irei redirecionar para o outro server em 8080.


```conf
http {
    server {
        listen 8080;
        root /etc/nginx/;
    }

    server {
        listen 8888;

        location / {
            proxy_pass http://localhost:8080/;
        }
    }
}

events { }
```

Agora você pode acessar tanto a rota `http://localhost:8080/` quanto `http://localhost:8888/` que vai cair no `index.html` do primeiro server.


# Load Balancer Layer 7

Para entender melhor um load balancer de layer 7, vá no meu material sobre o assunto:

- [Proxy](https://gitlab.com/e3666/backend-engineer/proxy)
- [LoadBalancer](https://gitlab.com/e3666/backend-engineer/load-balancer#load-balancer-layer-7)

## Exemplo 01

[Arquivos do exemplo...](https://gitlab.com/e3666/backend-engineer/load-balancer/-/tree/main/L7)

Para esse primeiro exemplo, eu vou simplesmente rotear uma request na porta 8080 para os dois servidores python, nas portas 5000 e 3000.

Veja que o nginx fica ouvindo na porta 8080, em /, e redireciona para o allbackend que é um bloco upstream que criei antes. E se você leu sobre load balancer layer 7, você entende porque o bloco principal é http.

O algoritmo padrão do nginx fará com que uma request vá para a 5000, e a seguinte para o 3000, dai volta para o 5000, e depois para o 3000. Ou seja, fica indo linha a linha do upstream, e depois volta.

```conf
http {
    upstream allbackend {
        server flask_app_1:5000;
        server flask_app_2:3000;
    }

    server {
        listen 8080;
        location / {
            proxy_pass http://allbackend/;
        }
    }
}

events { }
```

<img src='./images/fig_01.gif'/>

## Exemplo 02

[Arquivos do exemplo...](./Load-Balancer-07/exemplo-02)

Será a mesma coisa, mas vou trocar o algoritmo par `ip_hash`. Que faz um hash do ip do client, e envia ele para um server com base nesse hash, logo o mesmo client sempre vai parar no mesmo server.

```conf
http {
    upstream allbackend {
        ip_hash;
        server flask_app_1:5000;
        server flask_app_2:3000;
    }

    server {
        listen 8080;
        location / {
            proxy_pass http://allbackend/;
        }
    }
}

events { }
```

<img src='./images/fig_02.gif'/>

Veja que agora não importa quantas vezes eu recarregue, o server é sempre o mesmo, isso porque meu ip não muda.


## Exemplo 03

[Arquivos do exemplo...](./Load-Balancer-07/exemplo-03)

Agora vou rotear os servers com base no caminho. E para ficar mais interessante, vou jogar dois servers para cada caminho

- Servers `flask_app_1` e `flask_app_2` para a rota `/img`
- Servers `flask_app_3` e `flask_app_4` para a rota `/pdf`

```conf
http {
    upstream img_backend {
        server flask_app_1:5000;
        server flask_app_2:4000;
    }

    upstream pdf_backend {
        server flask_app_3:3000;
        server flask_app_4:2000;
    }

    server {
        listen 8080;
        location /img {
            proxy_pass http://img_backend/;
        }
        location /pdf {
            proxy_pass http://pdf_backend/;
        }
    }
}

events { }
```

<img src='./images/fig_03.gif'/>

Veja que ficamos trocando entre os servers 1 e 2, para a rota img, e 2 3 para a rota pdf. E eu precisei só adicionar as duas locations e o proxy pass para cada rota.


## Exemplo 04

[Arquivos do exemplo...](./Load-Balancer-07/exemplo-04)

Agora vou bloquear uma rota específica, como a rota /admin, para isso vou manter o código python e a ideia de dois servers para uma rota, e só vou adicionar o bloqueio, que consiste apenas de adicionar essa location e retornar 403 (forbidden).

```conf
http {
    upstream img_backend {
        server flask_app_1:5000;
        server flask_app_2:4000;
    }

    upstream pdf_backend {
        server flask_app_3:3000;
        server flask_app_4:2000;
    }

    server {
        listen 8080;
        location /img {
            proxy_pass http://img_backend/;
        }
        location /pdf {
            proxy_pass http://pdf_backend/;
        }
        location /admin {
            return 403;
        }
    }
}

events { }
```

<img src='./images/fig_04.gif'/>

# Load Balancer Layer 4

Para entender melhor um load balancer de layer 7, vá no meu material sobre o assunto:

- [Proxy](https://gitlab.com/e3666/backend-engineer/proxy)
- [LoadBalancer](https://gitlab.com/e3666/backend-engineer/load-balancer/-/tree/main/#load-balancer-layer-4)


A única coisa que vai mudar do layer 7 para cá, é que agora não vamos mais usar http, e sim stream. E que por ser layer 4, não podemos rotear pela rota, logo não podemos mais usar location.

## Exemplo 01

[Arquivos do exemplo...](https://gitlab.com/e3666/backend-engineer/load-balancer/-/tree/main/L4)


Vou copiar o exemplo 1 do layer 7 e trocar para stream, e remover o location, e o proxy pass não pode mais usar http.

```conf
stream {
    upstream allbackend {
        server flask_app_1:5000;
        server flask_app_2:3000;
    }

    server {
        listen 8080;
        proxy_pass allbackend;
    }
}

events { }
```

Veja que tudo que da para fazer é ouvir em uma porta, e jogar para outro server. Não da para fazer nada quanto a rota.

<img src='./images/fig_05.gif'/>

> OBS.: Como a proxy de layer 4 estabelece uma conexão tcp diretamente entre o server e o cliente, então é possível que no seu caso não fique trocando de servidor a todo momento igual ao meu.


# Publicando aplicação

Para essa parte você precisará de habilitar as portas 80 e 443 do seu roteador, ou usar um provedor de cloud, no meu caso, eu vou estar em uma instância EC2 amz-linux.

Vou utilizar o exemplo 4 do layer 7 para ser minha aplicação, porém o que mudarei, será o apontamento para a porta 80 ou em vez da 8080. E vou mudar os nomes dos containers python, mas isso não vai afetar nada no nosso exemplo.

## Instalando Docker

Considerando que você configurou o ec2 corretamente para expor as portas 80 e 443, você pode instalar o docker e o docker compose na instância. Basta seguir o passo a passo:

- Instalar docker AMZ linux
    ```sh
    $ sudo yum update -y
    $ sudo amazon-linux-extras install docker
    $ sudo service docker start
    $ sudo usermod -a -G docker ec2-user

    # saida do ssh e volte

    $ docker info # se isso executar, então deu certo de executar sem ser root
    ```

- Instalar docker compose:
    ```sh
    $ sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

    $ sudo chmod +x /usr/local/bin/docker-compose
    ```

## Exemplo 01

[Arquivos do exemplo...](./Publicar/exemplo-01)

Aqui eu vou simplesmente instânciar os contêineres na aws, sem configurar nada de DNS, http, criptografia, nada, só vou fazer a aplicação ficar acessível.

Então para essa primeira parte, o arquivo do nginx não muda nada:

```conf
http {
    upstream img_backend {
        server server_img_1:5000;
        server server_img_2:4000;
    }

    upstream pdf_backend {
        server server_pdf_1:3000;
        server server_pdf_2:2000;
    }

    server {
        listen 80;
        location /img {
            proxy_pass http://img_backend/;
        }
        location /pdf {
            proxy_pass http://pdf_backend/;
        }
        location /admin {
            return 403;
        }
    }
}

events { }
```

Você pode copiar o conteúdo da pasta [exemplo-01](./Publicar/exemplo-01)) para dentro dessa instância, [aqui tem a forma de fazer via ssh](https://angus.readthedocs.io/en/2014/amazon/transfer-files-between-instance.html), e executar o comando:

```sh
$ docker-compose up
```

Assim, você já será capaz de acessar a aplicação via ip público com http. No meu caso: `http://54.165.89.220/pdf` ou `http://54.165.89.220/img`

> OBS.: Eu já destruí essa instância, então não adianta tentar fazer requests nesse ip.

<img src='./images/fig_06.gif'/>


## Domínio

Mas agora, vamos criar um domínio no site [no ip](https://www.noip.com/pt-BR) que é de graça, basta fazer o cadastro lá. Quando seu cadastro estiver completo, vá no caminho indicado na imagem:

<img src='./images/fig_07.png'/>

Agora você preenche os campos com um hostname e aponte o IPv4 Address para sua máquina, ou para sua instência EC2, no meu caso, a instância EC2

<img src='./images/fig_08.png'/>

Agora você já consegue acessar a máquina com o domínio (pode precisar esperar algum tempo para o dns se espalhar, como meu IP é de uma EC2, então eu acredito que isso fez ser bem mais rápido) `http://mynginxteste.ddns.net/img`

<img src='./images/fig_09.gif'/>

## Criando certificado HTTPs

Agora vamos fazer o site ser https, para isso vamos usar o [letsencrypt](https://letsencrypt.org/pt-br/), e para fazer o serviço, vamos utilizar um programa deles que já automatiza o processo. Vou executar o programa dentro de um container, lá no EC2, assim fica tudo mais simples.

Para isso, você deve pausar o container do nginx, pois o certbot (programa do letsencrypt) utiliza a porta 80 para fazer a comunicação com os servidores do letsencrypt, então finalize o docker da aplicação. E execute o comando que vai estar abaixo, e ele vai começar a te perguntar algumas coisas:

- Aceita os termos? - sim
- Entre com o domínio: `mynginxteste.ddns.net`

```sh
$ sudo docker run -it --rm --name certbot \
            -v "/etc/letsencrypt:/etc/letsencrypt" \
            -v "/var/lib/letsencrypt:/var/lib/letsencrypt" \
            -p 80:80 \
            certbot/certbot certonly --standalone --register-unsafely-without-email
```

Agora espere terminar de executar, e você verá nos logs, que a chaves estarão no diretório `/etc/letsencrypt/live/mynginxteste.ddns.net/`. Tanto a chave pública `fullchain.pem` quanto a chave privada `privkey.pem`. E eu vou copiá-las para o meu diretório de trabalho na máquina ec2 somente para facilitar, pois assim elas vão parar para dentro do container do docker (já que estou compartilhando volume), que é para onde elas devem ir:

```sh
$ sudo cp /etc/letsencrypt/live/mynginxteste.ddns.net/fullchain.pem .
$ sudo cp /etc/letsencrypt/live/mynginxteste.ddns.net/privkey.pem .
```

## Exemplo 02

[Arquivos do exemplo...](./Publicar/exemplo-02)

Agora vamos configurar o nginx para receber requests HTTPS, com HTTP2.0, com TSL1.3. Para isso vamos primeiro de tudo, fazer com que o container do nginx espelhe a porta 443, e depois vamos configurar o arquivo `nginx.conf`:

Indo por partes, se você quiser apenas https você vai precisar configurar o server assim::

```conf
    server {
        listen 80;
        listen 443 ssl;

        ssl_certificate /etc/nginx/fullchain.pem;
        ssl_certificate_key /etc/nginx/privkey.pem;
```

Agora se quiser http2.0, então adicione http2 na refrente do ssl

```conf
    server {
        listen 80;
        listen 443 ssl http2;

        ssl_certificate /etc/nginx/fullchain.pem;
        ssl_certificate_key /etc/nginx/privkey.pem;
```

Agora se quiser configurar o protocolo tls1.3 então basta adicionar a linha `ssl_protocols TLSv1.3;` E por fim o arquivo fica da seguinte forma:


```conf
http {
    upstream img_backend {
        server server_img_1:5000;
        server server_img_2:4000;
    }

    upstream pdf_backend {
        server server_pdf_1:3000;
        server server_pdf_2:2000;
    }

    server {
        listen 80;
        listen 443 ssl http2;

        ssl_certificate /etc/nginx/fullchain.pem;
        ssl_certificate_key /etc/nginx/privkey.pem;

        ssl_protocols TLSv1.3;

        location /img {
            proxy_pass http://img_backend/;
        }
        location /pdf {
            proxy_pass http://pdf_backend/;
        }
        location /admin {
            return 403;
        }
    }
}

events { }
```

Nesse momento, você pode acessar o site, ver na devtools que o protocolo é http2, e consultar no site [cdn77](https://www.cdn77.com/tls-test) qual versão do tls que está configurada.

<img src='./images/fig_10.gif'/>


# Referência

- [NginX Crash Course (Layer 4 & Layer 7 Proxy, HTTPS, TLS 1.3, HTTP/2 & More)](https://www.youtube.com/watch?v=WC2-hNNBWII&list=PLQnljOFTspQWGuRmwojJ6LiV0ejm6eOcs&index=8)