from os import getenv
from flask import Flask

app = Flask(__name__)
server_name = getenv('SERVER_NAME')
server_port = getenv('SERVER_PORT')

@app.route('/')
def main():
    return f'servidor python {server_name}'

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=server_port)